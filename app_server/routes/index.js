var express = require('express');
var router = express.Router();

/* GET home page. */
const homepagecontroller = (req, res,) => {
  res.render('index', { title: 'Express' });
};
router.get('/', homepagecontroller);

module.exports = router;
